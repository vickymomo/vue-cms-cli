import Vue from 'vue'
import Router from 'vue-router'

// 导入首页组件
import HomeContainer from "@/components/tabbar/HomeContainer.vue";
// 新闻列表
import NewsList from "@/components/news/NewsList.vue";
// 新闻详情
import NewsDetails from "@/components/news/NewsDetails.vue";
// 图片列表
import PhotoList from "@/components/photo/PhotoList.vue";
// 图片详情
import PhotoDetails from "@/components/photo/PhotoDetails.vue";
// 商品列表
import GoodsList from "@/components/goods/GoodsList.vue";
// 商品详情
import GoodsDetails from "@/components/goods/GoodsDetails.vue";
import GoodsDesc from "@/components/goods/GoodsDesc.vue";
import GoodsComment from "@/components/goods/GoodsComment.vue";


// 导入会员相关组件
import MemberContainer from "@/components/tabbar/MemberContainer.vue";

// 导入购物车相关组件
import CartContainer from "@/components/tabbar/CartContainer.vue";

// 导入搜索相关组件
import SearchContainer from "@/components/tabbar/SearchContainer.vue";

Vue.use(Router)



export default new Router({
  routes:  [
    { path: "/", redirect: "/home" },
    { path: "/home", component: HomeContainer },
    { path: "/news", redirect: "/news/list" },
    { path: "/news/list", component: NewsList },
    { path: "/news/details/:id", component: NewsDetails },
    { path: "/photo", redirect: "/photo/list" },
    { path: "/photo/list", component: PhotoList },
    { path: "/photo/details/:id", component: PhotoDetails },
    { path: "/goods", redirect: "/goods/list" },
    { path: "/goods/list", component: GoodsList },
    { path: "/goods/details/:id", component: GoodsDetails, name: "goodsDetails" },
    { path: "/goods/desc/:id", component: GoodsDesc, name: "goodsDesc" },
    { path: "/goods/comment/:id", component: GoodsComment, name: "goodsComment" },
    { path: "/member", component: MemberContainer },
    { path: "/cart", component: CartContainer },
    { path: "/search", component: SearchContainer }
  ],
  linkActiveClass: "mui-active"
})
