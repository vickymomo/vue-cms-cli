// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";

Vue.config.productionTip = false;

// 导入时间插件
import moment from "moment";
// 定义全局过滤器
Vue.filter("dateFormat", function(
  dateStr,
  pattern = "YYYY年MM月DD日 HH:mm:ss"
) {
  return moment(dateStr).format(pattern);
});

// 导入 vue-resource
import VueResource from "vue-resource";
// 安装 vue-resource
Vue.use(VueResource);
// 设置请求的根路径
// Vue.http.options.root = "http://47.106.176.169:3001";
Vue.http.options.root = "http://127.0.0.1:3001";
// Vue.http.options.root = "http://192.168.8.111:3001";
// 全局设置 post 表单数据组织形式 application/x-www-form-urlencoded
Vue.http.options.emulateJSON = true;

// 导入 MUI 的样式
// 使用import导入后打包报错，从在index.html中用传统的方式引入该css文件
// import "../static/lib/mui/css/mui.min.css";
import "../static/lib/mui/css/icons-extra.css";

// 按需导入 Mint-UI 中的组件（按需加载时 LazyLoad 图标无法导入）
/* import { Header, Swipe, SwipeItem, Button, Lazyload } from "mint-ui";
Vue.component(Header.name, Header);
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);
Vue.component(Button.name, Button);
Vue.use(Lazyload); */
import MintUI from "mint-ui";
Vue.use(MintUI);
import "mint-ui/lib/style.min.css";

// 图片预览
import VuePreview from "vue-preview";
Vue.use(VuePreview);

// Vuex相关配置
import Vuex from "vuex";
Vue.use(Vuex);
// 进入页面首先从本地存储中获取购物车数据
var cart = JSON.parse(localStorage.getItem("cart") || "[]");
var store = new Vuex.Store({
  state: {
    // this.$store.state.***
    cart: cart // 购物车中的商品，其中存储商品对象 {id : 商品id, count : 要购买的商品数量, price : 商品单价, selected : false }
  },
  mutations: {
    // this.$store.commit('方法名称'[,唯一参数])
    addToCart(state, goods) {
      // 1.如果之前购物车中已经有了该商品，则只需要更新数量即可
      // 2.如果没有改商品，则直接把数据 push 到 cart 中即可
      var flag = false;
      state.cart.some(item => {
        if (item.id == goods.id) {
          item.count += parseInt(goods.count);
          flag = true;
          return true;
        }
      });
      if (!flag) state.cart.push(goods);
      // 当更新 cart 后，将 cart 数组存储到本地的 localStorage
      localStorage.setItem("cart", JSON.stringify(state.cart));
    },
    removeGoods(state, goodsId) {
      // 从购物车移除某种商品
      state.cart.splice(state.cart.findIndex(item => item.id == goodsId), 1);
      localStorage.setItem("cart", JSON.stringify(state.cart));
    },
    modifyGoods(state, obj) {
      // 修改购物车商品数量
      state.cart.some(item => {
        if (item.id == obj.goodsId) {
          if (obj.count) item.count = obj.count;
          if (obj.selected) item.selected = obj.selected;
          localStorage.setItem("cart", JSON.stringify(state.cart));
          return true;
        }
      });
    }
  },
  getters: {
    // this.$store.getters.***
    getCartTotalCount(state) {
      // 计算购物车商品总数
      var sum = 0;
      state.cart.forEach(element => {
        sum += element.count;
      });
      return sum;
    },
    getCartSelectedGoodsInfo(state) {
      var goodsInfo = { count: 0, amount: 0 };
      state.cart.forEach(item => {
        if (item.selected == true) {
          goodsInfo.count += item.count;
          goodsInfo.amount += item.count * item.price;
        }
      });
      return goodsInfo;
    }
  }
});

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  components: { App },
  template: "<App/>",
  store
});
